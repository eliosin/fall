![](https://elioway.gitlab.io/eliosin/sins/gluttony/elio-sins-gluttony-logo.png)

# gluttony

A [sins](https://gitlab.com/sins) theme.

![](https://elioway.gitlab.io/eliosin/sins/gluttony/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
