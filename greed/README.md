![](https://elioway.gitlab.io/eliosin/sins/greed/elio-sins-greed-logo.png)

# greed

A [sins](https://gitlab.com/sins) theme.

![](https://elioway.gitlab.io/eliosin/sins/greed/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
