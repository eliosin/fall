![](https://elioway.gitlab.io/eliosin/sins/pride/elio-sins-pride-logo.png)

# pride

A [sins](https://gitlab.com/sins) theme.

![](https://elioway.gitlab.io/eliosin/sins/pride/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
